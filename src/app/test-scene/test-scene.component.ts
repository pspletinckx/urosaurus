import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import * as THREE from 'three';
import { AbstractThreeComponent } from '../abstract-three/abstract-three.component';

@Component({
  selector: 'app-test-scene',
  templateUrl: './test-scene.component.html',
  styleUrls: ['./test-scene.component.css']
})
export class TestSceneComponent extends AbstractThreeComponent {

    @ViewChild('canvas')
    public canvasRef: ElementRef = {nativeElement : {}};
  
    protected get canvas(): HTMLCanvasElement {
      return this.canvasRef.nativeElement;
  }

  private scene = new THREE.Scene()
  private camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
  private cube = this.createCube();

  protected createScene() {
    this.scene.add(this.cube)
    this.camera.position.z = 5;
  }

  private createCube(): THREE.Mesh {
    const geometry = new THREE.BoxGeometry( 1, 1, 1 );
    const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
    const cube = new THREE.Mesh( geometry, material );
    return cube;
  }

  public animate() {
      this.cube.rotation.x += 0.01;
      this.cube.rotation.y += 0.01;
      this.renderer.render( this.scene, this.camera );
  }
}
