import { Component, OnInit, ViewChild, ElementRef, Injectable } from '@angular/core';
import * as THREE from 'three';

@Injectable()
export abstract class AbstractThreeComponent {
  
  protected renderer = new THREE.WebGLRenderer(); 
  
  protected abstract get canvas(): HTMLCanvasElement;
  protected abstract createScene(): void;
  public abstract animate(): void;

  private createRenderer() {
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvas,
      antialias: true
    });
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  }
  ngAfterViewInit() {
    this.createRenderer();
    this.createScene();
    this.bootstrapAnimate()
  }
  
  private bootstrapAnimate() {
    const component: AbstractThreeComponent = this;
    (function render() {
        requestAnimationFrame(render);
        component.animate();
    }());
  }
}
  
