export class CharacterMovement {
    moveLeft:boolean;
    moveRight:boolean;
    moveForward: boolean;
    moveBackward: boolean;
    jump?: boolean;
    crouch?: boolean;
}