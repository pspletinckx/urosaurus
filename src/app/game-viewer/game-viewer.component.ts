import { Component, ElementRef, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import * as THREE from 'three';
import { Material, MeshPhongMaterial, PlaneGeometry } from 'three';
// const TREE = require('three');
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { MD2CharacterComplex } from 'three/examples/jsm/misc/MD2CharacterComplex';
import { Gyroscope } from 'three/examples/jsm/misc/Gyroscope';
import { AbstractThreeComponent } from '../abstract-three/abstract-three.component';
import { PlayableCharacter } from '../playable-character';

@Component({
  selector: 'app-game-viewer',
  templateUrl: './game-viewer.component.html',
  styleUrls: ['./game-viewer.component.css']
})
export class GameViewerComponent extends AbstractThreeComponent{

    private camera: THREE.PerspectiveCamera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 1000);
    private clock = new THREE.Clock();
    public scene: THREE.Scene = new THREE.Scene();
    private dude?: PlayableCharacter;

  @ViewChild('canvas')
  public canvasRef: ElementRef = {nativeElement : {}};

  protected get canvas(): HTMLCanvasElement {
      return this.canvasRef.nativeElement;
  }

  protected createScene(): THREE.Scene {
      this.scene.background = new THREE.Color(0xFFFFFF);
      this.scene.fog = new THREE.Fog( 0xffffff, 1000, 4000 );
      this.camera = this.createSceneCamera();
      this.scene.add(this.camera);
      this.scene.add(new THREE.AmbientLight(0x222222));
      const directionalLight = this.createDirectionalLight();
      this.scene.add(directionalLight);
      this.scene.add(new THREE.AxesHelper(20));
      this.scene.add(new THREE.Group());

      this.scene.add(this.createGround())
      
      this.setUpCameraControls()

      const gyro = new Gyroscope();
      gyro.add( this.camera );
      gyro.add( directionalLight, directionalLight.target );
      this.dude = new PlayableCharacter(gyro);
      this.scene.add(this.dude.root);
      return this.scene
  }

  private createSceneCamera() : THREE.PerspectiveCamera {
    const camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 4000);
    camera.position.set( 0, 150, 1300 );
    return camera;
  }

  private createDirectionalLight(): THREE.DirectionalLight {
    const light = new THREE.DirectionalLight( 0xffffff, 2.25 );
    light.position.set( 200, 450, 500 );
    light.castShadow = true;

    light.shadow.mapSize.width = 1024;
    light.shadow.mapSize.height = 512;

    light.shadow.camera.near = 100;
    light.shadow.camera.far = 1200;

    light.shadow.camera.left = - 1000;
    light.shadow.camera.right = 1000;
    light.shadow.camera.top = 350;
    light.shadow.camera.bottom = - 350;
    return light;
  }

  private createGround(): THREE.Mesh {
    const gt = new THREE.TextureLoader().load( 'assets/textures/terrain/grasslight-big.jpg' );
    const gg = new THREE.PlaneGeometry( 16000, 16000 );
    const gm = new THREE.MeshPhongMaterial( { color: 0xffffff, map: gt } );

    const ground: THREE.Mesh <PlaneGeometry, MeshPhongMaterial> = new THREE.Mesh( gg, gm );
    ground.rotation.x = - Math.PI / 2;
    if(ground.material.map) {
        ground.material.map.repeat.set( 64, 64 );
        ground.material.map.wrapS = THREE.RepeatWrapping;
        ground.material.map.wrapT = THREE.RepeatWrapping;
        ground.material.map.encoding = THREE.sRGBEncoding;
    }
    // note that because the ground does not cast a shadow, .castShadow is left false
    ground.receiveShadow = true;
    return ground;
  }

  private setUpCameraControls() : void {
    const cameraControls = new OrbitControls( this.camera, this.renderer.domElement );
    cameraControls.target.set( 0, 50, 0 );
    cameraControls.update();
  }

  animate() {
    // ... minions do stuff
    const delta = this.clock.getDelta();
    this.dude?.update(delta);
    this.renderer.render( this.scene, this.camera );
  }



  /* EVENTS */

  public onMouseDown(event: MouseEvent) {
      console.log('onMouseDown');
      event.preventDefault();

      // Example of mesh selection/pick:
      const raycaster = new THREE.Raycaster();
      const mouse = new THREE.Vector2();
      mouse.x = (event.clientX / this.renderer.domElement.clientWidth) * 2 - 1;
      mouse.y = - (event.clientY / this.renderer.domElement.clientHeight) * 2 + 1;
      raycaster.setFromCamera(mouse, this.camera);

      const obj: THREE.Object3D[] = [];
      this.findAllObjects(obj, this.scene);
      const intersects = raycaster.intersectObjects(obj);
      console.log('Scene has ' + obj.length + ' objects');
      console.log(intersects.length + ' intersected objects found');
    //   intersects.forEach((i) => {
    //       console.log(i.object); // do what you want to do with object
    //   });

  }

  // public onKeyDown( event: Event ) {
  //   if (event instanceof KeyboardEvent) {
  //     this.dude?.onKeyDown(event.code);
  //   }

  // }

  // public onKeyUp( event: KeyboardEvent ) {
  //   this.dude?.onKeyUp(event.code);
  // }


  private findAllObjects(pred: THREE.Object3D[], parent: {children : Array<THREE.Object3D>}) {
      // NOTE: Better to keep separate array of selected objects
      if (parent.children.length > 0) {
          parent.children.forEach((i) => {
              pred.push(i);
              this.findAllObjects(pred, i);
          });
      }
  }

  public onMouseUp(event: MouseEvent) {
      console.log('onMouseUp');
  }

  // @HostListener('window:resize', ['$event'])
  // public onResize(event: Event) {
  //     this.canvas.style.width = '100%';
  //     this.canvas.style.height = '100%';
  //     console.log('onResize: ' + this.canvas.clientWidth + ', ' + this.canvas.clientHeight);

  //     this.camera.aspect = this.getAspectRatio();
  //     this.camera.updateProjectionMatrix();
  //     this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
  //     this.render();
  // }

  @HostListener('document:keypress', ['$event'])
  public onKeyPress(event: KeyboardEvent) {
      console.log('onKeyPress: ' + event.key);
  }

  @HostListener('document:keydown', ['$event'])
  public onKeyDown(event: KeyboardEvent) {
      console.log('onKeyDown: ' + event.key);
      this.dude?.onKeyDown(event.code);
  }

  @HostListener('document:keyup', ['$event'])
  public onKeyUp(event: KeyboardEvent) {
      console.log('onKeyDown: ' + event.key);
      this.dude?.onKeyUp(event.code);
  }
}
