import { MD2CharacterComplex } from './character/MD2CharacterComplex';
import { Gyroscope } from 'three/examples/jsm/misc/Gyroscope';

export class PlayableCharacter extends MD2CharacterComplex { 

    constructor(gyro: Gyroscope) {
        super();

        const configOgro = {

            baseUrl: 'assets/models/md2/ogro/',

            body: 'ogro.md2',
            skins: ['grok.jpg', 'freedom.png'],
            weapons: [['weapon.md2', 'weapon.jpg']],
            animations: {
                move: 'run',
                idle: 'stand',
                jump: 'jump',
                attack: 'attack',
                crouchMove: 'cwalk',
                crouchIdle: 'cstand',
                crouchAttach: 'crattack'
            },

            walkSpeed: 350,
            crouchSpeed: 175

        };

        this.scale = 3;
        // this.controls = 
        // this.shareParts( baseCharacter ); // load common

        // cast and receive shadows
        this.enableShadows(true);

        this.setWeapon(0);
        this.setSkin(1);

        this.root.position.x = 0;
        this.root.position.z = 0;

        this.root.add(gyro);

        this.loadParts(configOgro);

        this.controls = {

            moveForward: false,
            moveBackward: false,
            moveLeft: false,
            moveRight: false,
        }
    }

    public onKeyDown(code: string) {

        if (this.controls) {

            switch (code) {

                case 'ArrowUp':
                case 'KeyW': this.controls.moveForward = true; break;

                case 'ArrowDown':
                case 'KeyS': this.controls.moveBackward = true; break;

                case 'ArrowLeft':
                case 'KeyA': this.controls.moveLeft = true; break;

                case 'ArrowRight':
                case 'KeyD': this.controls.moveRight = true; break;

                // case 'KeyC': controls.crouch = false; break;
                // case 'Space': controls.jump = false; break;
                // case 'ControlLeft':
                // case 'ControlRight': controls.attack = false; break;
            }
        }

    }

    public onKeyUp(code: string) {

        switch (code) {

            case 'ArrowUp':
            case 'KeyW': this.controls.moveForward = false; break;

            case 'ArrowDown':
            case 'KeyS': this.controls.moveBackward = false; break;

            case 'ArrowLeft':
            case 'KeyA': this.controls.moveLeft = false; break;

            case 'ArrowRight':
            case 'KeyD': this.controls.moveRight = false; break;
        }
    }
}
