import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameViewerComponent } from './game-viewer/game-viewer.component';
import { TestSceneComponent } from './test-scene/test-scene.component';

@NgModule({
  declarations: [
    AppComponent,
    GameViewerComponent,
    TestSceneComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
